# sel4-targets

These are [target
specifications](https://github.com/rust-lang/rfcs/blob/master/text/0131-target-specification.md)
suitable for cross-compiling Rust crates to seL4. Copy the JSON files into
your `RUST_TARGET_PATH` (and set that environment to someplace, if you haven't).

For a more comprehensive description of setting up a build environment, see
[the docs](https://robigalia.org/docs/build-environment).

## Status

Complete for x86. Untested for ARM.